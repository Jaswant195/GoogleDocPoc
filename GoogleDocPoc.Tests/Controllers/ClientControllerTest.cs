﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoogleDocPoc.Controllers;
using GoogleDocPoc.Models;
using Telerik.JustMock;
using GoogleDocPoc.ViewModel;

namespace GoogleDocPoc.Tests.Controllers
{
    [TestClass]
    public class ClientControllerTest
    {
        [TestMethod]
        public void ClientDetails_Get_ShouldReturnView()
        {
            //Arrange
            var clientRepository = Mock.Create<IClientRepository>();
            Mock.Arrange(() => clientRepository.GetClientDetails()).Returns(new List<ClientViewModel>
            {
                new ClientViewModel()
                {
                    Id= 1,
                    Name="IBM"
                }
            }.ToList()).MustBeCalled();

            //Act
            ClientController controller = new ClientController(clientRepository);
            var result = controller.ClientDetails(null) as ViewResult;

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddClient_Success_ShouldReturnInteger()
        {
            //Arrange
            Client objClient = new Client();
            objClient.Id = 5;
            objClient.Name = "HCL";

            var clientRepository = Mock.Create<IClientRepository>();
            Mock.Arrange(() => clientRepository.AddClient(objClient)).Returns(1).MustBeCalled();

            //Act
            ClientController controller = new ClientController(clientRepository);
            var result = controller.AddClient(objClient) as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(true, result.Data.ToString().Contains("1"));
        }

        [TestMethod]
        public void AddClient_Failure_ShouldReturnInteger()
        {
            //Arrange
            Client objClient = new Client();
            objClient.Id = 5;
            objClient.Name = "HCL";

            var clientRepository = Mock.Create<IClientRepository>();
            Mock.Arrange(() => clientRepository.AddClient(objClient)).Returns(2).MustBeCalled();

            //Act
            ClientController controller = new ClientController(clientRepository);
            var result = controller.AddClient(objClient) as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(true, result.Data.ToString().Contains("2"));
        }

        [TestMethod]
        public void UpdateClient_Success_ShouldReturnInteger()
        {
            //Arrange
            ClientViewModel objClientViewModel = new ClientViewModel();
            objClientViewModel.Id = 5;
            objClientViewModel.Name = "HCL";
            objClientViewModel.HdnName = "HCL1";

            var clientRepository = Mock.Create<IClientRepository>();
            Mock.Arrange(() => clientRepository.UpdateClient(5,"HCL")).Returns(1).MustBeCalled();

            //Act
            ClientController controller = new ClientController(clientRepository);
            var result = controller.UpdateClient(objClientViewModel) as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(true, result.Data.ToString().Contains("1"));
        }

        [TestMethod]
        public void UpdateClient_Failure_ShouldReturnInteger()
        {
            //Arrange
            ClientViewModel objClientViewModel = new ClientViewModel();
            objClientViewModel.Id = 5;
            objClientViewModel.Name = "HCL";
            objClientViewModel.HdnName = "HCL1";

            var clientRepository = Mock.Create<IClientRepository>();
            Mock.Arrange(() => clientRepository.UpdateClient(5, "HCL")).Returns(2).MustBeCalled();

            //Act
            ClientController controller = new ClientController(clientRepository);
            var result = controller.UpdateClient(objClientViewModel) as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(true, result.Data.ToString().Contains("2"));
        }

        [TestMethod]
        public void DeleteClient_Success_ShouldReturnInteger()
        {
            //Arrange
            Client objClient = new Client();
            objClient.Id = 5;
            objClient.Name = "HCL";

            var clientRepository = Mock.Create<IClientRepository>();
            Mock.Arrange(() => clientRepository.DeleteClient(objClient)).Returns(1).MustBeCalled();

            //Act
            ClientController controller = new ClientController(clientRepository);
            var result = controller.DeleteClient(objClient) as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(true, result.Data.ToString().Contains("1"));
        }

        [TestMethod]
        public void DeleteClient_Failure_ShouldReturnInteger()
        {
            //Arrange
            Client objClient = new Client();
            objClient.Id = 5;
            objClient.Name = "HCL";

            var clientRepository = Mock.Create<IClientRepository>();
            Mock.Arrange(() => clientRepository.DeleteClient(objClient)).Returns(2).MustBeCalled();

            //Act
            ClientController controller = new ClientController(clientRepository);
            var result = controller.DeleteClient(objClient) as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(true, result.Data.ToString().Contains("2"));
        }
    }
}
