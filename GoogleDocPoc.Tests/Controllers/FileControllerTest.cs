﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoogleDocPoc;
using GoogleDocPoc.Controllers;
using GoogleDocPoc.Models;
using Telerik.JustMock;

namespace GoogleDocPoc.Tests.Controllers
{
    [TestClass]
    public class FileControllerTest
    {
        [TestMethod]
        public void Index_Get_ShouldReturnView()
        {
            //Arrange
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.GetFileDetails()).Returns(new List<FileDetails>()
            {
                new FileDetails()
                {
                FileId = 14,
                FileName = "PAYSLIP (1).PDF",
                ClientName = "IBM",
                ClientId = 11,
                FolderName = "JWALAK"
                }
            }.ToList()).MustBeCalled();

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.Index(null) as ViewResult;

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetFileDetailsById_Get_ShouldReturnView()
        {
            //Arrange
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.GetFileDetails()).Returns(new List<FileDetails>()
            {
                new FileDetails(){FileId = 14,FileName = "PAYSLIP.PDF",ClientName = "IBM",ClientId = 11,FolderName = "JWALAK"
            }}.ToList());
            Mock.Arrange(() => fileRepository.GetClientDetails()).Returns(new List<FileDetails>()
            {
                new FileDetails(){ClientName = "IBM",ClientId = 11},
                new FileDetails(){ClientName = "TCS",ClientId = 12}
            }.ToList());
            Mock.Arrange(() => fileRepository.GetFolderDetails(11)).Returns(new List<FileDetails>()
            {
                new FileDetails(){FolderId = 5,FolderName = "JWALAK"},
                new FileDetails(){FolderId = 6,FolderName = "JWALAMK"}
            }.ToList());

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.GetFileDetailsById(14) as PartialViewResult;

            //Assert
            Assert.AreEqual("_GridEditPartial", result.ViewName);
        }

        [TestMethod]
        public void DeleteFile_Success_ShouldReturnActionNameAndControllerName()
        {
            //Arrange
            var fileRepository = Mock.Create<IFileRepository>();

            Mock.Arrange(() => fileRepository.DeleteFileByFileId(14)).Returns(true).MustBeCalled();
            Mock.Arrange(() => fileRepository.DeleteFileFromFolder("abc", "abc", "abc.docs")).Returns(true).MustBeCalled();

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.DeleteFile(14, "abc", "abc", "abc.doc") as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("File", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void DeleteFile_Failure_ShouldReturnViewWithError()
        {
            //Arrange
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.DeleteFileByFileId(14)).Returns(false).MustBeCalled();

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.DeleteFile(14, "abc", "abc", "abc.doc") as ViewResult;

            //Assert
            Assert.AreEqual("Index",result.ViewName);
            Assert.AreEqual("Unable to delete the selected file", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }

        [TestMethod]
        public void AddFilePopup_Get_ShouldReturnView()
        {
            //Arrange
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.GetClientDetails()).Returns(new List<FileDetails>()
            {
                new FileDetails(){ClientName = "IBM",ClientId = 11},
                new FileDetails(){ClientName = "TCS",ClientId = 12},
                new FileDetails(){ClientName = "HCL",ClientId = 13}

            }.ToList());

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.AddFilePopup() as PartialViewResult;

            //Assert
            Assert.AreEqual("_FileAddPartial", result.ViewName);
        }

        [TestMethod]
        public void AddFilePopup_NoClientInDatabase_ShouldReturnActionNameAndControllerName()
        {
            //Arrange
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.GetClientDetails()).Returns((List<FileDetails>)null);

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.AddFilePopup() as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("File", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void GetFolderDetails_Get_ShouldReturnJsonResult()
        {
            //Arrange
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.GetFolderDetails(11)).Returns(new List<FileDetails>()
            {
                new FileDetails(){FolderId = 5,FolderName = "JWALAK"},
                new FileDetails(){FolderId = 6,FolderName = "JWALAMK"}
            }.ToList());

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.GetFolderDetails(11) as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
        }

        [TestMethod]
        public void AddUpdateFile_Get_ShouldReturnActionNameAndControllerName()
        {
            //Arrange
            FormCollection collection = new FormCollection();
            collection.Add("ClientDetails","11");
            collection.Add("FolderDetails", "5");
            collection.Add("SelectedClientName", "IBM");
            collection.Add("SelectedFolderName", "JWALAK");

            //Creating Mock Object
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.CheckFileExistance(11, 5, Arg.IsAny<string>())).Returns(false).MustBeCalled();
            Mock.Arrange(() => fileRepository.InsertFile(11, 5, Arg.IsAny<string>())).Returns(true).MustBeCalled();

            var Files = Mock.Create<System.Web.HttpPostedFileBase>();
            Mock.Arrange(() => Files.ContentLength).Returns(1);
            Mock.Arrange(() => Files.FileName).Returns("test.txt");

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.AddUpdateFile(collection,Files) as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("File", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void AddUpdateFile_LargeFileSize_ShouldReturnViewWithError()
        {
            //Arrange
            FormCollection collection = new FormCollection();
            collection.Add("ClientDetails", "11");
            collection.Add("FolderDetails", "5");
            collection.Add("SelectedClientName", "IBM");
            collection.Add("SelectedFolderName", "JWALAK");

            //Creating Mock Object
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.CheckFileExistance(11, 5, Arg.IsAny<string>())).Returns(false).MustBeCalled();
            Mock.Arrange(() => fileRepository.InsertFile(11, 5, Arg.IsAny<string>())).Returns(true).MustBeCalled();

            var Files = Mock.Create<System.Web.HttpPostedFileBase>();
            Mock.Arrange(() => Files.ContentLength).Returns(10000000);
            Mock.Arrange(() => Files.FileName).Returns("test.txt");

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.AddUpdateFile(collection, Files) as ViewResult;

            //Assert
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual("The size of the file should not exceed 5 MB", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }

        [TestMethod]
        public void AddUpdateFile_FileNotInserted_ShouldReturnViewWithError()
        {
            //Arrange
            FormCollection collection = new FormCollection();
            collection.Add("ClientDetails", "11");
            collection.Add("FolderDetails", "5");
            collection.Add("SelectedClientName", "IBM");
            collection.Add("SelectedFolderName", "JWALAK");

            //Creating Mock Object
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.CheckFileExistance(11, 5, Arg.IsAny<string>())).Returns(false).MustBeCalled();
            Mock.Arrange(() => fileRepository.InsertFile(11, 5, Arg.IsAny<string>())).Returns(false).MustBeCalled();

            var Files = Mock.Create<System.Web.HttpPostedFileBase>();
            Mock.Arrange(() => Files.ContentLength).Returns(1000);
            Mock.Arrange(() => Files.FileName).Returns("test.txt");

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.AddUpdateFile(collection, Files) as ViewResult;

            //Assert
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual("Record not inserted", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }

        [TestMethod]
        public void AddUpdateFile_FileExistsForSelectedClientAndFolder_ShouldReturnViewWithError()
        {
            //Arrange
            FormCollection collection = new FormCollection();
            collection.Add("ClientDetails", "11");
            collection.Add("FolderDetails", "5");
            collection.Add("SelectedClientName", "IBM");
            collection.Add("SelectedFolderName", "JWALAK");

            //Creating Mock Object
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.CheckFileExistance(11, 5, Arg.IsAny<string>())).Returns(true).MustBeCalled();
            Mock.Arrange(() => fileRepository.InsertFile(11, 5, Arg.IsAny<string>())).Returns(true).MustBeCalled();

            var Files = Mock.Create<System.Web.HttpPostedFileBase>();
            Mock.Arrange(() => Files.ContentLength).Returns(1000);
            Mock.Arrange(() => Files.FileName).Returns("test.txt");

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.AddUpdateFile(collection, Files) as ViewResult;

            //Assert
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual("File name with same client and folder is already exists.", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }

        [TestMethod]
        public void AddUpdateFile_InvalidExtention_ShouldReturnViewWithError()
        {
            //Arrange
            FormCollection collection = new FormCollection();
            collection.Add("ClientDetails", "11");
            collection.Add("FolderDetails", "5");
            collection.Add("SelectedClientName", "IBM");
            collection.Add("SelectedFolderName", "JWALAK");

            //Creating Mock Object
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.CheckFileExistance(11, 5, Arg.IsAny<string>())).Returns(false).MustBeCalled();
            Mock.Arrange(() => fileRepository.InsertFile(11, 5, Arg.IsAny<string>())).Returns(true).MustBeCalled();

            var Files = Mock.Create<System.Web.HttpPostedFileBase>();
            Mock.Arrange(() => Files.ContentLength).Returns(1000);
            Mock.Arrange(() => Files.FileName).Returns("test.ttxt");

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.AddUpdateFile(collection, Files) as ViewResult;

            //Assert
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual("Please select file with following extention - ppt, pptx, txt, pdf, docx, doc", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }

        [TestMethod]
        public void AddUpdateFile_UpdateFile_ShouldReturnViewWithError()
        {
            //Arrange
            FormCollection collection = new FormCollection();
            collection.Add("FileId", "19");
            collection.Add("myClientDetails", "11");
            collection.Add("MyFolderDetails", "5");
            collection.Add("SelectedClientName", "IBM");
            collection.Add("SelectedFolderName", "JWALAKM");

            //Creating Mock Object
            var fileRepository = Mock.Create<IFileRepository>();
            Mock.Arrange(() => fileRepository.CheckFileExistance(11, 5, Arg.IsAny<string>())).Returns(false).MustBeCalled();
            Mock.Arrange(() => fileRepository.InsertFile(11, 5, Arg.IsAny<string>())).Returns(true).MustBeCalled();
            Mock.Arrange(() => fileRepository.GetOldFileDetails(19)).Returns(new FileDetails
            {
                FileName = "PAYSLIP.PDF",
                ClientName = "IBM",
                FolderName = "JWALAK",
                CreatedDate= Convert.ToDateTime("2017-09-18 19:56:12.163")
            }).MustBeCalled();
            Mock.Arrange(() => fileRepository.DeleteFileFromFolder("IBM", "JWALAK", "PAYSLIP.PDF")).Returns(true).MustBeCalled();
            Mock.Arrange(() => fileRepository.UpdateFile(19,11,5,"PAYSLIP1.PDF", Convert.ToDateTime("2017-09-18 19:56:12.163"))).Returns(true).MustBeCalled();


            var Files = Mock.Create<System.Web.HttpPostedFileBase>();
            Mock.Arrange(() => Files.ContentLength).Returns(1000);
            Mock.Arrange(() => Files.FileName).Returns("PAYSLIP1.PDF");

            //Act
            FileController controller = new FileController(fileRepository);
            var result = controller.AddUpdateFile(collection, Files) as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("File", result.RouteValues["controller"]);
        }
    }
}
