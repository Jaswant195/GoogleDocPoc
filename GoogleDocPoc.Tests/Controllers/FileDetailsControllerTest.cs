﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoogleDocPoc.Controllers;
using GoogleDocPoc.Models;
using Telerik.JustMock;
using GoogleDocPoc.ViewModel;

namespace GoogleDocPoc.Tests.Controllers
{
    [TestClass]
    public class FileDetailsControllerTest
    {
        [TestMethod]
        public void Index_GetFileDetails_ShouldReturnView()
        {
            //Arrange
            var fileDetailsRepository = Mock.Create<IFileDetailsRepository>();
            Mock.Arrange(() => fileDetailsRepository.GetAllFileDetails("SuperUser", 1, 23)).Returns(new List<FilePath>
            {
                new FilePath()
                {
                 ClientId = 1,
                 ClientName = "TCS",
                 FolderId = 11,
                 FolderName = "ASDF",
                 FileName = "Financial Process.docx"
                }
            }.ToList()).MustBeCalled();
            Mock.Arrange(() => fileDetailsRepository.GetAllClientDetails()).Returns(new List<Client>()
            {
                new Client(){Name = "IBM",Id = 11},
                new Client(){Name = "TCS",Id = 1}
            }.ToList());
            //Act
            FileDetailsController controller = new FileDetailsController(fileDetailsRepository);
            var result = controller.Index() as ViewResult;

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void FormTreeview_Get_ShouldReturnJsonResult()
        {
            //Arrange
            var fileDetailsRepository = Mock.Create<IFileDetailsRepository>();
            Mock.Arrange(() => fileDetailsRepository.GetFilesByFileName(11, 5, "51660661.pdf", "2017-09-29 18:23:45.677", "2017-10-03 18:23:45.677")).Returns(new List<FilePath>
            {
                new FilePath()
                {
                 ClientId = 11,
                 ClientName = "IBM",
                 FolderId = 5,
                 FolderName = "JWALAK",
                 FileName = "51660661.pdf"
                }
            }.ToList()).MustBeCalled();

            //Act
            FileDetailsController controller = new FileDetailsController(fileDetailsRepository);
            var result = controller.FormTreeview(11, 5, "51660661.pdf", "2017-09-29 18:23:45.677", "2017-10-03 18:23:45.677") as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
        }

        [TestMethod]
        public void BindFolderDetails()
        {
            //Arrange
            var fileDetailsRepository = Mock.Create<IFileDetailsRepository>();
            Mock.Arrange(() => fileDetailsRepository.BindFolderDetails(11)).Returns(new List<Folder>
            {
                new Folder(){ID = 5, FolderName = "JWALAK", ClientId = 11},
                new Folder(){ID = 6, FolderName = "JWALAMK", ClientId = 11}
            }.ToList()).MustBeCalled();

            //Act
            FileDetailsController controller = new FileDetailsController(fileDetailsRepository);
            var result = controller.BindFolderDetails(11) as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
        }
    }
}
