﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoogleDocPoc.Controllers;
using GoogleDocPoc.Models;
using Telerik.JustMock;
using GoogleDocPoc.ViewModel;

namespace GoogleDocPoc.Tests.Controllers
{
    [TestClass]
    public class FolderControllerTest
    {
        [TestMethod]
        public void FolderDetails_Get_ShouldReturnView()
        {
            //Arrange
            var folderRepository = Mock.Create<IFolderRepository>();
            Mock.Arrange(() => folderRepository.GetFolderDetails()).Returns(new List<FolderViewModel>
            {
                new FolderViewModel()
                {
                    Id=5,
                    FolderName="JWALAK",
                    ClientId=11,
                    ClientName="IBM"
                }
            }.ToList()).MustBeCalled();

            //Act
            FolderController controller = new FolderController(folderRepository);
            var result = controller.FolderDetails(null) as ViewResult;

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void BindClientDetails_Get_ShouldReturnJsonResult()
        {
            //Arrange
            var folderRepository = Mock.Create<IFolderRepository>();
            Mock.Arrange(() => folderRepository.BindClientDetails()).Returns(new List<Client>
            {
                new Client(){Id=1, Name="HCL"},
                new Client(){Id=2, Name="CSC"}
            }.ToList()).MustBeCalled();

            //Act
            FolderController controller = new FolderController(folderRepository);
            var result = controller.BindClientDetails() as JsonResult;

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddFolder_Success_ShouldReturnJsonResult()
        {
            //Arrange
            FolderViewModel objFolderViewModel = new FolderViewModel();
            objFolderViewModel.Id = 1;
            objFolderViewModel.FolderName = "Folder";
            objFolderViewModel.ClientId = 11;
            objFolderViewModel.ClientName = "IBM";

            var folderRepository = Mock.Create<IFolderRepository>();
            Mock.Arrange(() => folderRepository.AddFolder(1, "Folder", 11)).Returns(1).MustBeCalled();

            //Act
            FolderController controller = new FolderController(folderRepository);
            var result = controller.AddFolder(objFolderViewModel) as JsonResult;
           
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Data.ToString().Contains("1"));
        }

        [TestMethod]
        public void AddFolder_Failure_ShouldReturnJsonResult()
        {
            //Arrange
            FolderViewModel objFolderViewModel = new FolderViewModel();
            objFolderViewModel.Id = 1;
            objFolderViewModel.FolderName = "Folder";
            objFolderViewModel.ClientId = 11;
            objFolderViewModel.ClientName = "IBM";

            var folderRepository = Mock.Create<IFolderRepository>();
            Mock.Arrange(() => folderRepository.AddFolder(1, "Folder", 11)).Returns(2).MustBeCalled();

            //Act
            FolderController controller = new FolderController(folderRepository);
            var result = controller.AddFolder(objFolderViewModel) as JsonResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Data.ToString().Contains("2"));
        }

        [TestMethod]
        public void UpdateFolder_Success_ShouldReturnJsonResult()
        {
            //Arrange
            FolderViewModel objFolderViewModel = new FolderViewModel();
            objFolderViewModel.Id = 1;
            objFolderViewModel.FolderName = "Folder";
            objFolderViewModel.ClientId = 11;

            var folderRepository = Mock.Create<IFolderRepository>();
            Mock.Arrange(() => folderRepository.UpdateFolder(1, "Folder", 11)).Returns(1).MustBeCalled();

            //Act
            FolderController controller = new FolderController(folderRepository);
            var result = controller.UpdateFolder(objFolderViewModel) as JsonResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Data.ToString().Contains("1"));
        }

        [TestMethod]
        public void UpdateFolder_Failure_ShouldReturnJsonResult()
        {
            //Arrange
            FolderViewModel objFolderViewModel = new FolderViewModel();
            objFolderViewModel.Id = 1;
            objFolderViewModel.FolderName = "Folder";
            objFolderViewModel.ClientId = 11;

            var folderRepository = Mock.Create<IFolderRepository>();
            Mock.Arrange(() => folderRepository.UpdateFolder(1, "Folder", 11)).Returns(2).MustBeCalled();

            //Act
            FolderController controller = new FolderController(folderRepository);
            var result = controller.UpdateFolder(objFolderViewModel) as JsonResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Data.ToString().Contains("2"));
        }

        [TestMethod]
        public void DeleteFolder_Success_ShouldReturnJsonResult()
        {
            //Arrange
            FolderViewModel objFolderViewModel = new FolderViewModel();
            objFolderViewModel.Id = 1;

            var folderRepository = Mock.Create<IFolderRepository>();
            Mock.Arrange(() => folderRepository.DeleteFolder(1)).Returns(1).MustBeCalled();

            //Act
            FolderController controller = new FolderController(folderRepository);
            var result = controller.DeleteFolder(objFolderViewModel) as JsonResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Data.ToString().Contains("1"));
        }

        [TestMethod]
        public void DeleteFolder_Failure_ShouldReturnJsonResult()
        {
            //Arrange
            FolderViewModel objFolderViewModel = new FolderViewModel();
            objFolderViewModel.Id = 1;

            var folderRepository = Mock.Create<IFolderRepository>();
            Mock.Arrange(() => folderRepository.DeleteFolder(1)).Returns(2).MustBeCalled();

            //Act
            FolderController controller = new FolderController(folderRepository);
            var result = controller.DeleteFolder(objFolderViewModel) as JsonResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Data.ToString().Contains("2"));
        }
    }
}
