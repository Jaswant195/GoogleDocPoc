﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoogleDocPoc;
using GoogleDocPoc.Controllers;
using GoogleDocPoc.Models;
using Telerik.JustMock;

namespace GoogleDocPoc.Tests.Controllers
{
    [TestClass]
    public class LoginControllerTest
    {
        [TestMethod]
        public void Login_TestGetMethod_ShouldReturnView()
        {
            // Arrange
            LoginController loginController = new LoginController();

            // Act
            ViewResult result = loginController.Login() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        ////Without Mocking
        //[TestMethod]
        //public void Login_ValidUsernameAndPassword_ShouldReturnView_NoMock()
        //{
        //    // Arrange
        //    User objUser = new User();
        //    objUser.Username = "jash";
        //    objUser.Password = "12345";

        //    // Act
        //    LoginController controller = new LoginController();
        //    var result = controller.Login(objUser) as RedirectToRouteResult;

        //    // Assert
        //    Assert.AreEqual("Index", result.RouteValues["action"]);
        //    Assert.AreEqual("File", result.RouteValues["controller"]);
        //}

        [TestMethod]
        public void Login_InvalidUsernameAndPassword_ShouldReturnView()
        {
            // Arrange
            User objUser = new User();
            objUser.Username = "jash12";
            objUser.Password = "12345";

            var loginRepository = Mock.Create<ILoginRepository>();
            Mock.Arrange(() => loginRepository.GetUserDetails(objUser)).Returns((UserDetails)null).MustBeCalled();

            // Act
            LoginController controller = new LoginController(loginRepository);
            ViewResult result = controller.Login(objUser) as ViewResult;

            // Assert
            Assert.AreEqual("Login", result.ViewName);
        }

        //Unit test method Using Just Mock
        [TestMethod]
        public void Login_ValidUser_ShouldReturnObject()
        {
            // Arrange
            User objUser = new User();
            objUser.Username = "jash";
            objUser.Password = "12345";

            var loginRepository = Mock.Create<ILoginRepository>();
            Mock.Arrange(() => loginRepository.GetUserDetails(objUser)).Returns(new UserDetails
            {
                UserId = 18,
                RoleName = "Admin",
                ClientName = "HCL",
                UserName = "Jash",
                ClientId = 1
            }).MustBeCalled();
            Mock.Arrange(() => loginRepository.GetUserActivationStatus(18)).Returns(false).MustBeCalled();

            // Act
            LoginController controller = new LoginController(loginRepository);
            var result = controller.Login(objUser) as RedirectToRouteResult;

            // Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("FileDetails", result.RouteValues["controller"]);
        }
        
        [TestMethod]
        public void Logout_Get_ShouldReturnActionNameAndControllerName()
        {
            //Arrange
            LoginController controller = new LoginController();

            //Act
            var result = controller.LogOut() as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("Login", result.RouteValues["action"]);
            Assert.AreEqual("Login", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void ForgotPassword_TestGetMethod_ShouldReturnView()
        {
            // Arrange
            LoginController loginController = new LoginController();

            // Act
            ViewResult result = loginController.ForgotPassword() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ForgotPassword_ValidUser_ShouldReturnViewWithSuccessMesssage()
        {
            // Arrange
            UserDetails userDetails = new UserDetails {UserName = "Jash",Password = "12345"};
            Forgotpassword Forgotpassword= new Forgotpassword {Email = "jashobanta.pradhan@kantaritp.com"};

            ILoginRepository loginRepository = Mock.Create<ILoginRepository>();

            Mock.Arrange(() => loginRepository.GetUserDetailsByEmail(Forgotpassword.Email)).Returns(new UserDetails
                                                                                    { UserName = "Jash", Password = "12345" }).MustBeCalled();

            Mock.Arrange(() => loginRepository.SendMail(Arg.IsAny<UserDetails>(), Arg.IsAny<string>())).Returns("Password send to your mail").MustBeCalled();

            // Act
            LoginController controller = new LoginController(loginRepository);
            var result = controller.ForgotPassword(Forgotpassword) as ViewResult;

            //// Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Your password has been send to your mail.", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }

        [TestMethod]
        public void ForgotPassword_ValidUser_ShouldReturnViewWithErrorMesssage()
        {
            // Arrange
            UserDetails userDetails = new UserDetails { UserName = "Jash", Password = "12345" };
            Forgotpassword Forgotpassword = new Forgotpassword { Email = "jashobanta.pradhan@kantaritp.com" };


            ILoginRepository loginRepository = Mock.Create<ILoginRepository>();

            Mock.Arrange(() => loginRepository.GetUserDetailsByEmail(Forgotpassword.Email)).Returns(new UserDetails
            { UserName = "Jash", Password = "12345" }).MustBeCalled();

            Mock.Arrange(() => loginRepository.SendMail(Arg.IsAny<UserDetails>(), Arg.IsAny<string>())).Returns("Issue sending email").MustBeCalled();

            // Act
            LoginController controller = new LoginController(loginRepository);
            var result = controller.ForgotPassword(Forgotpassword) as ViewResult;

            //// Assert
            Assert.IsNotNull(result);
            Assert.AreNotEqual("Password send to your mail", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }

        [TestMethod]
        public void ForgotPassword_InValidUser_ShouldReturnViewWithErrorMesssage()
        {
            // Arrange
            Forgotpassword Forgotpassword = new Forgotpassword { Email = "jashobanta.pradhan12@kantaritp.com" };

            // Act
            ILoginRepository loginRepository = Mock.Create<ILoginRepository>();

            Mock.Arrange(() => loginRepository.GetUserDetailsByEmail(Arg.IsAny<string>())).Returns((UserDetails)null).MustBeCalled();

            LoginController controller = new LoginController(loginRepository);
            var result = controller.ForgotPassword(Forgotpassword) as ViewResult;

            //// Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("No user found by this email", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }
    }
}
