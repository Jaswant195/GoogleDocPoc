﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoogleDocPoc.Controllers;
using GoogleDocPoc.Models;
using Telerik.JustMock;
using GoogleDocPoc.ViewModel;

namespace GoogleDocPoc.Tests.Controllers
{
    [TestClass]
    public class RegistrationControllerTest
    {
        [TestMethod]
        public void UserDetails_Get_ShouldReturnView()
        {
            //Arrange
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.GetAllUserDetails("SuperUser", "IBM", "Admin")).Returns(new List<UserDetailsViewModel>
            {
                new UserDetailsViewModel()
                {
                 UserId = 1,
                 Username = "Admin", 
                 Password = "12345",
                 Email = "Admin@gmail.com",
                 ClientId = 11,
                 RoleName = "SuperUser",
                 ClientName = "IBM",
                 FolderIds = "5,6"
                }
            }.ToList()).MustBeCalled();

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.UserDetails(null) as ViewResult;

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CheckUserNameForDuplication_UserExists_ShouldReturnTrue()
        {
            //Arrange
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.CheckUserExists("Admin")).Returns(true);

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.CheckUserNameForDuplication("Admin") as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(true,result.Data,"Return value should be true.");
        }

        [TestMethod]
        public void CheckUserNameForDuplication_UserNotExists_ShouldReturnFalse()
        {
            //Arrange
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.CheckUserExists("Admin12")).Returns(false);

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.CheckUserNameForDuplication("Admin12") as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(false, result.Data, "Return value should be true.");
        }

        [TestMethod]
        public void CheckEmailForDuplication_EmailExists_ShouldReturnTrue()
        {
            //Arrange
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.CheckEmailExists("Admin@gmail.com")).Returns(true);

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.CheckEmailForDuplication("Admin@gmail.com") as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(true, result.Data, "Return value should be true.");
        }

        [TestMethod]
        public void CheckEmailForDuplication_EmailNotExists_ShouldReturnFalse()
        {
            //Arrange
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.CheckEmailExists("Admin12@gmail.com")).Returns(false);

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.CheckEmailForDuplication("Admin12@gmail.com") as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(false, result.Data, "Return value should be true.");
        }

        [TestMethod]
        public void GetFolderDetails_UserRegistration_ShouldReturnJsonResult()
        {
            //Arrange
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.GetFolderDetails(11)).Returns(new List<FileDetails>()
            {
                new FileDetails(){FolderId = 2,FolderName = "JWALAK"},
                new FileDetails(){FolderId = 3,FolderName = "JWALAMK"}
            }.ToList());

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.GetFolderDetails("11") as JsonResult;

            //Assert
            Assert.IsNotNull(result.Data);
        }

        [TestMethod]
        public void DeleteUser_Success_ShouldReturnActionNameAndControllerName()
        {
            //Arrange
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.DeleteUserByUserId(18)).Returns(true);

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.DeleteUser(18) as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("UserDetails", result.RouteValues["action"]);
            Assert.AreEqual("Registration", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void DeleteUser_Failure_ShouldReturnViewWithErrorMessage()
        {
            //Arrange
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.DeleteUserByUserId(18)).Returns(false);

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.DeleteUser(18) as ViewResult;

            //Assert
            Assert.AreEqual("UserDetails", result.ViewName);
            Assert.AreEqual("Unable to delete the selected user", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }

        [TestMethod]
        public void AddUser_Success_ShouldReturnViewNameWithControllerName()
        {
            //Arrange
            FormCollection collection = new FormCollection();
            collection.Add("UserName", "Admin");
            collection.Add("Password", "12345");
            collection.Add("Email", "Admin@gmail.com");
            collection.Add("RoleDetails", "SuperUser");
            collection.Add("ClientDetails", "IBM");
            collection.Add("SelectedFolderIds", "2,3");

            //Creating Mock Object
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.InsertUser("Admin", "12345", "Admin@gmail.com", "SuperUser", "IBM", "2,3")).Returns(true);

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.AddUser(collection) as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("UserDetails", result.RouteValues["action"]);
            Assert.AreEqual("Registration", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void AddUser_Failure_ShouldReturnViewNameWithError()
        {
            //Arrange
            FormCollection collection = new FormCollection();
            collection.Add("UserName", "Admin");
            collection.Add("Password", "12345");
            collection.Add("Email", "Admin@gmail.com");
            collection.Add("RoleDetails", "SuperUser");
            collection.Add("ClientDetails", "IBM");
            collection.Add("SelectedFolderIds", "2,3");

            //Creating Mock Object
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.InsertUser("Admin", "12345", "Admin@gmail.com", "SuperUser", "IBM", "2,3")).Returns(false);

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.AddUser(collection) as ViewResult;

            //Assert
            Assert.AreEqual("UserDetails", result.ViewName);
            Assert.AreEqual("Record not inserted", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }

        [TestMethod]
        public void UpdateUser_Success_ShouldReturnViewNameWithControllerName()
        {
            //Arrange
            FormCollection collection = new FormCollection();
            collection.Add("UserId", "1");
            collection.Add("UserName", "Admin");
            collection.Add("Password", "12345");
            collection.Add("Email", "Admin@gmail.com");
            collection.Add("MyRoleDetails", "SuperUser");
            collection.Add("MyClientDetails", "IBM");
            collection.Add("MySelectedFolderIds", "2,3");

            //Creating Mock Object
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.UpdateUser("1","Admin", "12345", "Admin@gmail.com", "SuperUser", "IBM", "2,3")).Returns(true);

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.UpdateUser(collection) as RedirectToRouteResult;

            //Assert
            Assert.AreEqual("UserDetails", result.RouteValues["action"]);
            Assert.AreEqual("Registration", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void UpdateUser_Failure_ShouldReturnViewNameWithControllerName()
        {
            //Arrange
            FormCollection collection = new FormCollection();
            collection.Add("UserId", "1");
            collection.Add("UserName", "Admin");
            collection.Add("Password", "12345");
            collection.Add("Email", "Admin@gmail.com");
            collection.Add("MyRoleDetails", "SuperUser");
            collection.Add("MyClientDetails", "IBM");
            collection.Add("MySelectedFolderIds", "2,3");

            //Creating Mock Object
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.UpdateUser("1", "Admin", "12345", "Admin@gmail.com", "SuperUser", "IBM", "2,3")).Returns(false);

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.UpdateUser(collection) as ViewResult;

            //Assert
            Assert.AreEqual("UserDetails", result.ViewName);
            Assert.AreEqual("Record not Updated", result.ViewData.ModelState["Error"].Errors.FirstOrDefault().ErrorMessage);
        }

        [TestMethod]
        public void AddUserDetails_Get_ShouldReturnPartialView()
        {
            //Arrange
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.GetAllClientDetails("jash")).Returns(new List<FileDetails>()
            {
                new FileDetails(){ClientName = "IBM",ClientId = 11},
                new FileDetails(){ClientName = "TCS",ClientId = 12}
            }.ToList());
            Mock.Arrange(() => registrationRepository.getAllRoles("SuperUser")).Returns(new List<UserDetailsViewModel>()
            {
                new UserDetailsViewModel(){RoleName = "Administrator",RoleId = 1},
                new UserDetailsViewModel(){RoleName = "SuperUser",RoleId = 3}
            }.ToList());

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.AddUserDetails() as PartialViewResult;

            //Assert
            Assert.AreEqual("_AddUserPartial", result.ViewName);
        }

        [TestMethod]
        public void GetUserDetailsById_Get_ShouldReturnPartialView()
        {
            //Arrange
            var registrationRepository = Mock.Create<IRegistrationRepository>();
            Mock.Arrange(() => registrationRepository.GetUserDetailsByUserId(1)).Returns(new UserDetailsViewModel()
                        {   UserId = 1,Username = "Admin",Password = "12345",ConfirmPassword = "12345",Email = "Admin@gmail.com",ClientId = 11,
                            RoleName = "SuperUser",ClientName = "IBM",FolderIds = "2,3"
                        }).MustBeCalled();
            Mock.Arrange(() => registrationRepository.GetAllClientDetails("jash")).Returns(new List<FileDetails>()
                        {   new FileDetails(){ClientName = "IBM",ClientId = 11},
                            new FileDetails(){ClientName = "TCS",ClientId = 12}
                        }.ToList());
            Mock.Arrange(() => registrationRepository.getAllRoles("SuperUser")).Returns(new List<UserDetailsViewModel>()
                        {
                            new UserDetailsViewModel(){RoleName = "Administrator",RoleId = 1},
                            new UserDetailsViewModel(){RoleName = "SuperUser",RoleId = 3}
                        }.ToList());

            //Act
            RegistrationController controller = new RegistrationController(registrationRepository);
            var result = controller.GetUserDetailsById(1) as PartialViewResult;

            //Assert
            Assert.AreEqual("_EditUserPartial", result.ViewName);
        }
    }
}
