﻿using GoogleDocPoc.Models;
using GoogleDocPoc.Models.Repository;
using GoogleDocPoc.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoogleDocPoc.Controllers
{
    public class ClientController : Controller
    {
        private readonly IClientRepository clientRepository;
        private ILog _ILog;

        public ClientController(IClientRepository clientRepository)
        {
            this.clientRepository = clientRepository;
        }

        public ClientController()
        {
            this.clientRepository = new ClientRepository();
            _ILog = Log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            _ILog.LogException(filterContext.Exception.ToString());
            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        public ActionResult ClientDetails(int? pageNumber)
        {
            ClientViewModel model = new ClientViewModel();
            model.PageNumber = (pageNumber == null ? 1 : Convert.ToInt32(pageNumber));
            model.PageSize = 4;

            List<ClientViewModel> clientDetails = clientRepository.GetClientDetails();
            if (clientDetails != null)
            {
                model.Clients = clientDetails.OrderBy(x => x.Id)
                         .Skip(model.PageSize * (model.PageNumber - 1))
                         .Take(model.PageSize).ToList();

                model.TotalCount = clientDetails.Count();
                var page = (model.TotalCount / model.PageSize) - (model.TotalCount % model.PageSize == 0 ? 1 : 0);
                model.PagerCount = page + 1;
            }
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                Session["Model"] = model;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddClient(Client client)
        {
           int i = clientRepository.AddClient(client);
            if (i == 1)
            {
                if (ConfigurationManager.AppSettings["path"] == "FromApp")
                {
                    string directoryPath = Server.MapPath("~/GoogleDocuments\\") + client.Name;
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }
                }
            }
            return Json(new { Id = i }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateClient(ClientViewModel clientModel)
        {
           int i = clientRepository.UpdateClient(clientModel.Id, clientModel.Name);
            if (i == 1)
            {
                if (ConfigurationManager.AppSettings["path"] == "FromApp")
                {
                    string path = Server.MapPath("~/GoogleDocuments");
                    string Fromfol = "\\" + clientModel.HdnName + "\\";
                    string Tofol = "\\" + clientModel.Name + "\\";
                    Directory.Move(path + Fromfol, path + Tofol);
                }
            }
            return Json(new { Id = i }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteClient(Client client)
        {
            int i = clientRepository.DeleteClient(client);
            if (i==1)
            {
                if (ConfigurationManager.AppSettings["path"] == "FromApp")
                {
                    string directoryPath = Server.MapPath("~/GoogleDocuments\\") + client.Name;
                    if (Directory.Exists(directoryPath))
                    {
                        Directory.Delete(directoryPath, true);
                    }
                }
            }
            return Json(new { Id = i }, JsonRequestBehavior.AllowGet);
        }
    }
}