﻿using GoogleDocPoc.Models;
using GoogleDocPoc.Models.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoogleDocPoc.Controllers
{
    public class FileController : Controller
    {
        private readonly IFileRepository fileRepository;
        private ILog _ILog;

        public FileController(IFileRepository fileRepository)
        {
            this.fileRepository = fileRepository;
        }

        public FileController()
        {
            this.fileRepository = new FileRepository();
            _ILog = Log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            _ILog.LogException(filterContext.Exception.ToString());
            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        // GET: File
        public ActionResult Index(int? pageNumber)
        {
            FileDetails model = new FileDetails();
            model.PageNumber = (pageNumber == null ? 1 : Convert.ToInt32(pageNumber));
            model.PageSize = 4;

            List<FileDetails> fileDetails = fileRepository.GetFileDetails();

            if (fileDetails != null)
            {
                model.FileDetail = fileDetails.OrderBy(x => x.FileId)
                         .Skip(model.PageSize * (model.PageNumber - 1))
                         .Take(model.PageSize).ToList();

                model.TotalCount = fileDetails.Count();
                var page = (model.TotalCount / model.PageSize) - (model.TotalCount % model.PageSize == 0 ? 1 : 0);
                model.PagerCount = page + 1;
            }
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                Session["Model"] = model;
            }
            return View(model);
        }

        public ActionResult GetFileDetailsById(int id)
        {
            FileDetails fileDetails = fileRepository.GetFileDetails().SingleOrDefault(x => x.FileId == id);
            var clientDetails = fileRepository.GetClientDetails();
            var folderDetails = fileRepository.GetFolderDetails(fileDetails.ClientId);

            var lstClientDetails = new List<SelectListItem>();
            foreach (var item in clientDetails)
            {
                lstClientDetails.Add(new SelectListItem { Value = item.ClientId.ToString(), Text = item.ClientName, Selected = (item.ClientName == fileDetails.ClientName) });
            }
            ViewBag.ClientDetails = lstClientDetails;

            var lstFolderDetails = new List<SelectListItem>();
            foreach (var item in folderDetails)
            {
                lstFolderDetails.Add(new SelectListItem { Value = item.FolderId.ToString(), Text = item.FolderName, Selected = (item.FolderName == fileDetails.FolderName) });
            }

            ViewBag.FolderDetails = lstFolderDetails;

            return PartialView("_GridEditPartial", fileDetails);
        }

        public ActionResult DeleteFile(int Id, string clientName, string folderName, string fileName)
        {
            bool IsFileDelete = fileRepository.DeleteFileByFileId(Id);

            if (IsFileDelete)
            {
               bool status = fileRepository.DeleteFileFromFolder(clientName, folderName, fileName);
            }
            else
            {
                ModelState.AddModelError("Error", "Unable to delete the selected file");
                if (ConfigurationManager.AppSettings["path"] == "FromApp")
                {
                    return View("Index", (FileDetails)Session["Model"]);
                }
                else
                {
                    return View("Index");
                }
            }
            TempData["FileOperationStatus"] = "Selected file deleted successfully";
            return RedirectToAction("Index", "File");
        }

        public ActionResult AddFilePopup()
        {
            var clientDetails = fileRepository.GetClientDetails();

            if (clientDetails != null)
            {
                var lstClientDetails = new List<SelectListItem>();
                foreach (var item in clientDetails)
                {
                    lstClientDetails.Add(new SelectListItem { Value = item.ClientId.ToString(), Text = item.ClientName });
                }
                ViewBag.ClientDetails = lstClientDetails;

                return PartialView("_FileAddPartial");
            }
            TempData["FileOperationStatus"] = "There Should be atleast one client in the database";
            return RedirectToAction("Index", "File");
        }

        public JsonResult GetFolderDetails(int id)
        {
            List<FileDetails> FolderDetails = fileRepository.GetFolderDetails(id);
            List<SelectListItem> folderDetail = new List<SelectListItem>();

            if (FolderDetails != null)
            {
                foreach (var item in FolderDetails)
                {
                    folderDetail.Add(new SelectListItem { Text = item.FolderName, Value = item.FolderId.ToString() });
                }
            }
            return Json(new SelectList(folderDetail, "Value", "Text"));
        }

        [HttpPost]
        public ActionResult AddUpdateFile(FormCollection model,HttpPostedFileBase Files)
        {
            HttpPostedFileBase postedFile;
            string ClientId = ""; string FolderId = ""; string ClientName = ""; string FolderName = "";

            string FileId = model["FileId"] == null ? "" : model["FileId"];

            if (FileId == "")
            {
                ClientId   = model["ClientDetails"];
                FolderId   = model["FolderDetails"];
                ClientName = model["SelectedClientName"];
                FolderName = model["SelectedFolderName"];
            }
            else
            {
                ClientId   = model["myClientDetails"];
                FolderId   = model["myFolderDetails"];
                ClientName = model["SelectedClientName"];
                FolderName = model["SelectedFolderName"];
            }

            if (ConfigurationManager.AppSettings["path"] == "FromApp"){
                postedFile = Request.Files[0];
            }
            else{
                postedFile = Files;
            }

            FileDetails fileModel = new FileDetails();
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                if (Session["Model"] != null){
                    fileModel = (FileDetails)Session["Model"];
                }
                else{
                    return RedirectToAction("Index", "File");
                }
            }

            //Validations for checking the File
            if (postedFile != null)
            {
                var fileSize = Math.Round(((decimal)postedFile.ContentLength / (decimal)1048576), 2);
                if (fileSize > 5)
                {
                    ModelState.AddModelError("Error", "The size of the file should not exceed 5 MB");
                    return View("Index", fileModel);
                }
                else
                {
                    var fileName = Path.GetFileName(postedFile.FileName);
                    string ext = Path.GetExtension(fileName).ToLower();
                    if ((new[] { ".ppt", ".pptx", ".txt", ".pdf", ".docx", ".doc" }).Contains(ext))
                    {
                        bool IsFileExists = fileRepository.CheckFileExistance(Convert.ToInt32(ClientId), Convert.ToInt32(FolderId), fileName);
                        if (!IsFileExists)
                        {
                            bool status = false;
                            if (FileId == "")
                            {
                                status = fileRepository.InsertFile(Convert.ToInt32(ClientId), Convert.ToInt32(FolderId), fileName);
                                TempData["FileOperationStatus"] = "File details Inserted successfully";
                            }
                            else
                            {
                                //Delete Old File
                                FileDetails objOldFileDetails = fileRepository.GetOldFileDetails(Convert.ToInt32(FileId));
                                bool ret = fileRepository.DeleteFileFromFolder(objOldFileDetails.ClientName, objOldFileDetails.FolderName, objOldFileDetails.FileName);

                                //Update file details
                                status = fileRepository.UpdateFile(Convert.ToInt32(FileId), Convert.ToInt32(ClientId), Convert.ToInt32(FolderId), fileName, objOldFileDetails.CreatedDate);
                                TempData["FileOperationStatus"] = "File details Updated successfully";
                            }

                            if (status)
                            {
                                if (ConfigurationManager.AppSettings["path"] == "FromApp")
                                {
                                    var path = Server.MapPath("~/GoogleDocuments\\") + ClientName + "\\" + FolderName + "\\";
                                    if (!Directory.Exists(path))
                                    {
                                        Directory.CreateDirectory(path);
                                    }
                                    postedFile.SaveAs(path + fileName);
                                }
                            }
                            else
                            {
                                ModelState.AddModelError("Error", "Record not inserted");
                                return View("Index", fileModel);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("Error", "File name with same client and folder is already exists.");
                            return View("Index", fileModel);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Error", "Please select file with following extention - ppt, pptx, txt, pdf, docx, doc");
                        return View("Index", fileModel);
                    }
                }
            }
            return RedirectToAction("Index","File");
        }
    }
}