﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using GoogleDocPoc.Models;
using GoogleDocPoc.Models.Repository;
using System.Configuration;

namespace GoogleDocPoc.Controllers
{
    public class FileDetailsController : Controller
    {
        private readonly IFileDetailsRepository fileDetailsRepository;
        private ILog _ILog;

        public FileDetailsController(IFileDetailsRepository fileDetailsRepository)
        {
            this.fileDetailsRepository = fileDetailsRepository;
        }

        public FileDetailsController()
        {
            this.fileDetailsRepository = new FileDetailsRepository();
            _ILog = Log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            _ILog.LogException(filterContext.Exception.ToString());
            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }
        // GET: FileDetails
        public ActionResult Index()
        {
            string roleName = "SuperUser"; int clientId = 1; int userId = 23;
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                roleName = Session["RoleName"].ToString();
                clientId = Convert.ToInt32(Session["ClientId"]);
                userId   = Convert.ToInt32(Session["UserId"]);
            }
           
            var filePathDetails = new FilePathDetails();
            filePathDetails.FromDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
            filePathDetails.ToDate = DateTime.Now.ToString("MM/dd/yyyy");
            Client obj = new Client {
                Id = 0,
                Name = "Please Select"
            };

            filePathDetails.FilePathDetail = fileDetailsRepository.GetAllFileDetails(roleName, clientId, userId);

            filePathDetails.Clients = fileDetailsRepository.GetAllClientDetails();
            filePathDetails.Clients.Insert(0, obj);

            return View(filePathDetails);
        }

        [HttpPost]
        public JsonResult FormTreeview(int ClientId, int FolderId, string FileName, string FromDate, string ToDate)
        {
            var filePathDetails = new FilePathDetails();

            filePathDetails.FilePathDetail = fileDetailsRepository.GetFilesByFileName(ClientId, FolderId, FileName, FromDate, ToDate);

            return Json(new { result = filePathDetails.FilePathDetail }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BindFolderDetails(int ClientId)
        {
            List<Folder> folderDetails = fileDetailsRepository.BindFolderDetails(ClientId);

            return Json(new { result = folderDetails }, JsonRequestBehavior.AllowGet);
        }
    }
}