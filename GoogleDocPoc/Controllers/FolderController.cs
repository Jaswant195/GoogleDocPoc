﻿using GoogleDocPoc.Models;
using GoogleDocPoc.Models.Repository;
using GoogleDocPoc.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoogleDocPoc.Controllers
{
    public class FolderController : Controller
    {
        private readonly IFolderRepository folderRepository;
        private ILog _ILog;

        public FolderController(IFolderRepository folderRepository)
        {
            this.folderRepository = folderRepository;
        }

        public FolderController()
        {
            this.folderRepository = new FolderRepository();
            _ILog = Log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            _ILog.LogException(filterContext.Exception.ToString());
            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        public ActionResult FolderDetails(int? pageNumber)
        {
            FolderViewModel model = new FolderViewModel();
            model.PageNumber = (pageNumber == null ? 1 : Convert.ToInt32(pageNumber));
            model.PageSize = 4;

            List<FolderViewModel> folderDetails = folderRepository.GetFolderDetails();
            if (folderDetails != null)
            {
                model.Folders = folderDetails.OrderBy(x => x.Id)
                         .Skip(model.PageSize * (model.PageNumber - 1))
                         .Take(model.PageSize).ToList();

                model.TotalCount = folderDetails.Count();
                var page = (model.TotalCount / model.PageSize) - (model.TotalCount % model.PageSize == 0 ? 1 : 0);
                model.PagerCount = page + 1;
            }
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                Session["Model"] = model;
            }
            return View(model);
        }

        public JsonResult BindClientDetails()
        {
            var clientDetails = folderRepository.BindClientDetails();
            return Json(new { result = clientDetails }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddFolder(FolderViewModel folderModel)
        {
            int i = folderRepository.AddFolder(folderModel.Id,folderModel.FolderName,folderModel.ClientId);
            if (i == 1)
            {
                if (ConfigurationManager.AppSettings["path"] == "FromApp")
                {
                    string directoryPath = Server.MapPath("~/GoogleDocuments\\") + folderModel.ClientName + "\\" + folderModel.FolderName;
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }
                }
            }
            return Json(new { Id = i }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateFolder(FolderViewModel folderModel)
        {
            int i = folderRepository.UpdateFolder(folderModel.Id, folderModel.FolderName, folderModel.ClientId);
            if (i == 1)
            {
                if (ConfigurationManager.AppSettings["path"] == "FromApp")
                {
                    string pathFrom = Server.MapPath("~/GoogleDocuments\\") + folderModel.HdnClientName;
                    string pathTo = Server.MapPath("~/GoogleDocuments\\") + folderModel.ClientName;
                    string Fromfol = "\\" + folderModel.HdnFolderName + "\\";
                    string Tofol = "\\" + folderModel.FolderName + "\\";
                    Directory.Move(pathFrom + Fromfol, pathTo + Tofol);
                }
            }
            return Json(new { Id = i }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteFolder(FolderViewModel folderModel)
        {
            int i = folderRepository.DeleteFolder(folderModel.Id);
            if (i == 1)
            {
                if (ConfigurationManager.AppSettings["path"] == "FromApp")
                {
                    string directoryPath = Server.MapPath("~/GoogleDocuments\\") + folderModel.ClientName + "\\" + folderModel.FolderName;
                    if (Directory.Exists(directoryPath))
                    {
                        Directory.Delete(directoryPath, true);
                    }
                }
            }
            return Json(new { Id = i }, JsonRequestBehavior.AllowGet);
        }
    }
}