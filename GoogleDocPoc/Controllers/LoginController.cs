using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoogleDocPoc.Models;
using System.Web.Security;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.Configuration;
using GoogleDocPoc.Models.Repository;

namespace GoogleDocPoc.Controllers
{
       public class LoginController : Controller
    {
        private readonly ILoginRepository loginRepository;
        private ILog _ILog;

        public LoginController(ILoginRepository loginRepository)
        {
            this.loginRepository = loginRepository;
        }

        public LoginController()
        {
            this.loginRepository = new LoginRepository();
            _ILog = Log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            _ILog.LogException(filterContext.Exception.ToString());
            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User objLogin)
        {
            if (ModelState.IsValid)
            {
                UserDetails obj = loginRepository.GetUserDetails(objLogin);

                if (obj != null)
                {
                    bool isUserActivated = loginRepository.GetUserActivationStatus(obj.UserId);

                    if (isUserActivated)
                    {
                        ModelState.AddModelError("Error", "User is not activated");
                        return View(objLogin);
                    }
                    if (ConfigurationManager.AppSettings["path"] == "FromApp")
                    {
                        Session["UserId"] = obj.UserId;
                        Session["RoleName"] = obj.RoleName;
                        Session["ClientName"] = obj.ClientName;
                        Session["UserName"] = obj.UserName;
                        Session["ClientId"] = obj.ClientId;
                    }
                    return RedirectToAction("Index", "FileDetails");
                }
                ModelState.AddModelError("Error", "Username/Password is wrong");
            }
            return View("Login", objLogin);
        }

        public ActionResult LogOut()
        {
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                FormsAuthentication.SignOut();
                Session.Abandon(); 
            }
            return RedirectToAction("Login", "Login");
        }

        // GET: Account/LostPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        // POST: Account/LostPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(Forgotpassword Forgotpassword)
        {
            if (ModelState.IsValid)
            {
                UserDetails userDetails = loginRepository.GetUserDetailsByEmail(Forgotpassword.Email);

                if (userDetails != null)
                {
                   string message = loginRepository.SendMail(userDetails, Forgotpassword.Email);

                    if (message == "Password send to your mail")
                    {
                        ModelState.AddModelError("Error", "Your password has been send to your mail.");
                    }
                    else
                    {
                        ModelState.AddModelError("Error", "Issue sending email: " + message);
                    }
                }
                else // Email not found
                {
                    ModelState.AddModelError("Error", "No user found by this email");
                }
            }
            return View(Forgotpassword);
        }
    }
}