﻿using GoogleDocPoc.Models;
using GoogleDocPoc.Models.Repository;
using GoogleDocPoc.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace GoogleDocPoc.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly IRegistrationRepository registrationRepository;
        private ILog _ILog;

        public RegistrationController(IRegistrationRepository registrationRepository)
        {
            this.registrationRepository = registrationRepository;
        }

        public RegistrationController()
        {
            this.registrationRepository = new RegistrationRepository();
            _ILog = Log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            _ILog.LogException(filterContext.Exception.ToString());
            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        // GET: Registration
        public ActionResult UserDetails(int? pageNumber)
        {
            string RoleName = "", ClientName = "", UserName = "";
            UserDetailsViewModel model = new UserDetailsViewModel();
            model.PageNumber = (pageNumber == null ? 1 : Convert.ToInt32(pageNumber));
            model.PageSize = 8;

            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                 RoleName   = Session["RoleName"].ToString();
                 ClientName = Session["ClientName"].ToString();
                 UserName   = Session["UserName"].ToString();
            }
            else
            {
                RoleName = "SuperUser"; ClientName = "IBM"; UserName = "Admin";
            }

            List<UserDetailsViewModel> userDetails = registrationRepository.GetAllUserDetails(RoleName, ClientName, UserName);

            if (userDetails != null)
            {
                model.userDetailsViewModel = userDetails //.OrderBy(x => sortedData + "ASC")
                         .Skip(model.PageSize * (model.PageNumber - 1))
                         .Take(model.PageSize).ToList();

                model.TotalCount = userDetails.Count();
                var page = (model.TotalCount / model.PageSize) - (model.TotalCount % model.PageSize == 0 ? 1 : 0);
                model.PagerCount = page + 1;
            }
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                Session["UserDetailsModel"] = model;
            }
            return View(model);
        }

        public JsonResult CheckUserNameForDuplication(string UserName)
        {
            bool status = registrationRepository.CheckUserExists(UserName);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckEmailForDuplication(string Email)
        {
            bool status = registrationRepository.CheckEmailExists(Email);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFolderDetails(string id)
        {
            int _id = string.IsNullOrEmpty(id) ? 0 : int.Parse(id);
            var FolderDetails = registrationRepository.GetFolderDetails(_id);
            string folderIds = "";
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                if (Session["UserDetailsById"] != null)
                {
                    UserDetailsViewModel model = (UserDetailsViewModel)Session["UserDetailsById"];
                    folderIds = model.FolderIds == null ? "" : model.FolderIds;
                }
            }
            else
            {
                folderIds = "2,3";
            }

            List<SelectListItem> folderDetail = new List<SelectListItem>();

            if (FolderDetails != null)
            {
                foreach (var item in FolderDetails)
                {
                    folderDetail.Add(new SelectListItem { Text = item.FolderName, Value = item.FolderId.ToString(), Selected = folderIds.Contains(item.FolderId.ToString()) });
                }
            }
            return Json(folderDetail, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteUser(int UserId)
        {
            bool status = registrationRepository.DeleteUserByUserId(UserId);

            if (status)
            {
                TempData["CURDOperationStatus"] = "User details deleted successfully from database";
                return RedirectToAction("UserDetails", "Registration");
            }
            else
            {
                ModelState.AddModelError("Error", "Unable to delete the selected user");

                if (ConfigurationManager.AppSettings["path"] == "FromApp")
                {
                    return View("UserDetails", (UserDetailsViewModel)Session["UserDetailsModel"]);
                }
                else
                {
                    return View("UserDetails");
                }
            }
        }

        [HttpPost]
        public ActionResult AddUser(FormCollection model)
        {
            string UserName = model["UserName"];
            string Password = model["Password"];
            string Email    = model["Email"];
            string Role     = model["RoleDetails"];
            string Client   = model["ClientDetails"];
            string FolderIds = model["SelectedFolderIds"];

            UserDetailsViewModel UserModel = new UserDetailsViewModel();
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                if (Session["UserDetailsModel"] != null)
                {
                    UserModel = (UserDetailsViewModel)Session["UserDetailsModel"];
                }
                else
                {
                    return RedirectToAction("UserDetails", "Registration");
                }
            }

            bool status = registrationRepository.InsertUser(UserName, Password, Email, Role, Client, FolderIds);
            if (status != true)
            {
                ModelState.AddModelError("Error", "Record not inserted");
                return View("UserDetails", UserModel);
            }
            TempData["CURDOperationStatus"] = "User Details Added successfully";
            return RedirectToAction("UserDetails", "Registration");
        }

        [HttpPost]
        public ActionResult UpdateUser(FormCollection model)
        {
            string UserId   = model["UserId"];
            string UserName = model["UserName"];
            string Password = model["Password"];
            string Email    = model["Email"];
            string Role     = model["MyRoleDetails"];
            string Client   = model["MyClientDetails"];
            string FolderIds = model["MySelectedFolderIds"];

            UserDetailsViewModel UserModel = new UserDetailsViewModel();
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                if (Session["UserDetailsModel"] != null)
                {
                    UserModel = (UserDetailsViewModel)Session["UserDetailsModel"];
                }
                else
                {
                    return RedirectToAction("UserDetails", "Registration");
                }
            }

            bool status = registrationRepository.UpdateUser(UserId, UserName, Password, Email, Role, Client, FolderIds);
            if (status != true)
            {
                ModelState.AddModelError("Error", "Record not Updated");
                return View("UserDetails", UserModel);
            }
            TempData["CURDOperationStatus"] = "User Details Updated successfully";
            return RedirectToAction("UserDetails", "Registration");
        }

        public ActionResult AddUserDetails()
        {
            var clientDetails = new List<FileDetails>();
            var Roles = new List<UserDetailsViewModel>();
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                 clientDetails = registrationRepository.GetAllClientDetails(Session["UserName"].ToString());
                 Roles = registrationRepository.getAllRoles(Session["RoleName"].ToString());
            }
            else
            {
                 clientDetails = registrationRepository.GetAllClientDetails("jash");
                Roles = registrationRepository.getAllRoles("SuperUser");
            }
            
            var lstClientDetails = new List<SelectListItem>();
            foreach (var item in clientDetails)
            {
                lstClientDetails.Add(new SelectListItem { Value = item.ClientId.ToString(), Text = item.ClientName });
            }
            ViewBag.ClientDetails = lstClientDetails;

            var lstRoleDetails = new List<SelectListItem>();
            foreach (var item in Roles)
            {
                lstRoleDetails.Add(new SelectListItem { Value = item.RoleId.ToString(), Text = item.RoleName });
            }
            ViewBag.RoleDetails = lstRoleDetails;

            return PartialView("_AddUserPartial");
        }

        public ActionResult GetUserDetailsById(int UserId)
        {
            var Roles = new List<UserDetailsViewModel>();
            UserDetailsViewModel model = registrationRepository.GetUserDetailsByUserId(UserId);
            var clientDetails = new List<FileDetails>();
            if (ConfigurationManager.AppSettings["path"] == "FromApp")
            {
                clientDetails = registrationRepository.GetAllClientDetails(Session["UserName"].ToString());
                Roles = registrationRepository.getAllRoles(Session["RoleName"].ToString());
                Session["UserDetailsById"] = model;
            }
            else
            {
                clientDetails = registrationRepository.GetAllClientDetails("jash");
                Roles = registrationRepository.getAllRoles("SuperUser");
            }

            var lstRoleDetails = new List<SelectListItem>();
            foreach (var item in Roles)
            {
                lstRoleDetails.Add(new SelectListItem { Value = item.RoleId.ToString(), Text = item.RoleName, Selected = (item.RoleName == model.RoleName) });
            }
            ViewBag.RoleDetails = lstRoleDetails;

            var lstClientDetails = new List<SelectListItem>();
            foreach (var item in clientDetails)
            {
                lstClientDetails.Add(new SelectListItem { Value = item.ClientId.ToString(), Text = item.ClientName, Selected = (item.ClientName == model.ClientName) });
            }
            ViewBag.ClientDetails = lstClientDetails;

            return PartialView("_EditUserPartial", model);
        }
    }
}