namespace GoogleDocPoc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clientidchanged : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Folders", "ClientId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Folders", "ClientId", c => c.Int());
        }
    }
}
