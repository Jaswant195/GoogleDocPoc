using System;
using System.Collections.Generic;

namespace GoogleDocPoc.Models
{
    public partial class File
    {
        public int FileId { get; set; }
        public Nullable<int> ClientId { get; set; }
        public Nullable<int> FolderId { get; set; }
        public string FileName { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
    }
}
