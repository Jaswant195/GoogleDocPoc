﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleDocPoc.Models
{
    public class FileDetails
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string ClientName { get; set; }
        public string FolderName { get; set; }
        public int FolderId { get; set; }
        public int ClientId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }

        //pagination
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int PagerCount { get; set; }

        public List<FileDetails> FileDetail { get; set; }
    }
}