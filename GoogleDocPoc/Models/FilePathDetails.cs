﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace GoogleDocPoc.Models
{
    public class FilePathDetails
    {

        public List<Client> Clients { get; set; }
        public List<FilePath> FilePathDetail { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

    }
    public class FilePath
    {
        public int? ClientId { get; set; }
        public string ClientName { get; set; }
        public int? FolderId { get; set; }
        public string FolderName { get; set; }
        public string FileName { get; set; }
    }
    public class FilePathDetailsTreeView
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public int ParentId { get; set; }
        public string FileExtention { get; set; }
        public List<FilePathDetailsTreeView> childrens { get; set; }
    }
}