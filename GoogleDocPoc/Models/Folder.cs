using System;
using System.Collections.Generic;

namespace GoogleDocPoc.Models
{
    public partial class Folder
    {
        public int ID { get; set; }
        public string FolderName { get; set; }

        public int ClientId { get; set; }
    }
}
