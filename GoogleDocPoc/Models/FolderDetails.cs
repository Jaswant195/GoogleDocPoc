﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleDocPoc.Models
{
    public class FolderDetails
    {
        public int FolderId { get; set; }
        public string FolderName { get; set; }
        public int? ClientId { get; set; }
        public string ClientName { get; set; }
    }
}