﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoogleDocPoc.Models
{
    public class Forgotpassword
    {
        [Required(ErrorMessage = "We need your email to send you the Password !")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Not a valid email--what are you trying to do here?")]
        public string Email { get; set; }
    }
}