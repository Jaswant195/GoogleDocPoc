using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace GoogleDocPoc.Models
{
    public partial class GoogleDocsContext : DbContext
    {
        static GoogleDocsContext()
        {
            //Database.SetInitializer<GoogleDocsContext>(null);
            Database.SetInitializer(new  CreateDatabaseIfNotExists<GoogleDocsContext>());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<GoogleDocsContext,Migrations.Configuration>(useSuppliedContext: true));

        }

        public GoogleDocsContext()
            : base("Name=GoogleDocsContext")
        {
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Folder> Folders { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<tblFile> tblFiles { get; set; }
        public DbSet<UserActivation> UserActivations { get; set; }
        public DbSet<UserFolder> UserFolders { get; set; }
        public DbSet<User> Users { get; set; }

    }
}
