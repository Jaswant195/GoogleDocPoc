﻿using GoogleDocPoc.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GoogleDocPoc.Models
{
    public class ClientRepository : IClientRepository
    {
        public List<ClientViewModel> GetClientDetails()
        {
            List<ClientViewModel> FileDetails = new List<ClientViewModel>();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                FileDetails = (from C in db.Clients select 
                                   new ClientViewModel { Id=C.Id,Name=C.Name}).ToList();
            }
            return FileDetails;
        }
        
        public int AddClient(Client client)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                try
                {
                   db.Clients.Add(client);
                   int i = db.SaveChanges();

                   return i;
                }
                catch (System.Exception ex)
                {
                    string error = ex.InnerException.InnerException.Message;
                    if (error.Contains("UNIQUE"))
                    {
                        return 2;
                    }
                    else
                    {
                        return 3;
                    }
                }

            }  
        }

        public int UpdateClient(int clientId, string ClientName)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                try
                {
                    Client updatedClient = (from c in db.Clients
                                            where c.Id == clientId
                                            select c).FirstOrDefault();
                    updatedClient.Name = ClientName;
                    int i = db.SaveChanges();
                    return i;
                }
                catch (System.Exception ex)
                {

                    string error = ex.InnerException.InnerException.Message;
                    if (error.Contains("UNIQUE"))
                    {
                        return 2;
                    }
                    else
                    {
                        return 3;
                    }

                }

            }
        }

        [HttpPost]
        public int DeleteClient(Client client)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                Client clientId = (from c in db.Clients
                                     where c.Id == client.Id
                                     select c).FirstOrDefault();
                db.Clients.Remove(clientId);
               int i = db.SaveChanges();
                return i;
            }
           
        }
    }
}