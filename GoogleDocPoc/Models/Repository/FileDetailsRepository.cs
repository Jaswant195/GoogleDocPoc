﻿using GoogleDocPoc.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace GoogleDocPoc.Controllers
{
    public class FileDetailsRepository : IFileDetailsRepository
    {
        public List<FilePath> GetAllFileDetails(string roleName , int clientId, int userId)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                object[] parameters = { new SqlParameter("@RoleName", SqlDbType.VarChar) { Value = roleName},
                                                new SqlParameter("@ClientId", SqlDbType.Int) { Value = clientId} ,
                                                new SqlParameter("@UserId", SqlDbType.Int) { Value = userId} };

                return db.Database.SqlQuery<FilePath>("exec Usp_GetAllFileDetails @RoleName,@ClientId,@UserId", parameters).ToList();

            }
        }

        public List<Client> GetAllClientDetails()
        {
            List<Client> objClient = new List<Client>();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                objClient = (from C in db.Clients
                             select C).ToList();
            }
            return objClient;
        }

        public List<FilePath> GetFilesByFileName(int ClientId, int FolderId, string FileName, string FromDate, string ToDate)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                object[] parameters = {
                    new SqlParameter("@ClientId", SqlDbType.Int) { Value=ClientId} ,
                    new SqlParameter("@FolderId", SqlDbType.Int) { Value=FolderId},
                    new SqlParameter("@FileName", SqlDbType.VarChar) { Value=FileName},
                    new SqlParameter("@FromDate", SqlDbType.VarChar) { Value=FromDate},
                    new SqlParameter("@ToDate", SqlDbType.VarChar) { Value=ToDate}};

                return db.Database.SqlQuery<FilePath>("exec GetFilesByFileName @ClientId,@FolderId,@FileName,@FromDate,@ToDate", parameters).ToList();
            }
        }

        public List<Folder> BindFolderDetails(int ClientId)
        {
            List<Folder> folderDetails = new List<Folder>();

            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                folderDetails = (from   F in db.Folders
                                 where  F.ClientId == ClientId
                                 select F).ToList();
            }
            return folderDetails;
        }
    }
}