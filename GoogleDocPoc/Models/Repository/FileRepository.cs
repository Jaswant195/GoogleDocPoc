﻿using GoogleDocPoc.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace GoogleDocPoc.Controllers
{
    public class FileRepository : IFileRepository
    {
        public List<FileDetails> GetFileDetails()
        {
            List<FileDetails> FileDetails = new List<Models.FileDetails>();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                FileDetails = (from F in db.Files
                               join Cl in db.Clients on F.ClientId equals Cl.Id
                               join Fo in db.Folders on F.FolderId equals Fo.ID
                               orderby Cl.Name, Fo.FolderName, F.FileName
                               select new FileDetails { FileId = F.FileId, FileName = F.FileName, ClientName = Cl.Name, ClientId = Cl.Id, FolderName = Fo.FolderName }).ToList();
            }
            return FileDetails;
        }

        public FileDetails GetOldFileDetails(int FileId)
        {
            FileDetails objFileDetails = new FileDetails();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                objFileDetails = (from F in db.Files
                           join Cl in db.Clients on F.ClientId equals Cl.Id
                           join Fo in db.Folders on F.FolderId equals Fo.ID
                           where F.FileId == FileId
                           select new FileDetails { FileName = F.FileName, ClientName = Cl.Name, FolderName = Fo.FolderName,CreatedDate=F.CreatedDate }).FirstOrDefault();
            }
            return objFileDetails;
        }
        
        public List<FileDetails> GetClientDetails()
        {
            List<FileDetails> FileDetails = new List<Models.FileDetails>();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                FileDetails = (from C in db.Clients
                               select new FileDetails { ClientId = C.Id, ClientName = C.Name }).ToList();
            }
            return FileDetails;
        }

        public List<FileDetails> GetFolderDetails(int Id)
        {
            List<FileDetails> FolderDetails = new List<Models.FileDetails>();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                FolderDetails = (from F in db.Folders
                                 where F.ClientId == Id
                                 select new FileDetails { FolderId = F.ID, FolderName = F.FolderName }).ToList();
            }
            return FolderDetails;
        }

        public bool CheckFileExistance(int ClientId, int FolderId, string fileName)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                if (db.Files.Where(x => x.ClientId == ClientId && x.FolderId == FolderId && x.FileName == fileName).Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool InsertFile(int ClientId, int FolderId, string fileName)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                Models.File objFile = new Models.File
                {
                    ClientId = ClientId,
                    FolderId = FolderId,
                    FileName = fileName,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                };
                
                try
                {
                    db.Files.Add(objFile);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public bool UpdateFile(int FileId, int ClientId, int FolderId, string fileName,DateTime? CreatedDate)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                Models.File objFile = new Models.File
                {
                    FileId=FileId,
                    ClientId = ClientId,
                    FolderId = FolderId,
                    FileName = fileName,
                    CreatedDate=CreatedDate,
                    UpdatedDate = DateTime.Now
                };

                try
                {
                    db.Files.Add(objFile);
                    db.Entry(objFile).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public bool DeleteFileFromFolder(string ClientName, string FolderName, string FileName)
        {
            string sourceDir = HttpContext.Current.Server.MapPath("~/GoogleDocuments\\") + ClientName + "\\" + FolderName + "\\";
            string[] Files = Directory.GetFiles(sourceDir);

            foreach (string file in Files)
            {
                string Name = file.Substring(file.LastIndexOf('\\') + 1, file.Length - file.LastIndexOf('\\') - 1);
                if (Name == FileName)
                {
                    System.IO.File.Delete(file);
                }
            }
            return true;
        }

        public bool DeleteFileByFileId(int FileId)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                    var objFile = (from File in db.Files
                             where File.FileId==FileId
                             select File).FirstOrDefault();
                    db.Files.Remove(objFile);
                    db.SaveChanges();
                    return true;
            }
        }
    }
}