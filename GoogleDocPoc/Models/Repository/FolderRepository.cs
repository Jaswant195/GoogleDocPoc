﻿using GoogleDocPoc.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoogleDocPoc.Models
{
	public class FolderRepository : IFolderRepository
	{
        public List<FolderViewModel> GetFolderDetails()
        {
            List<FolderViewModel> FolderDetails = new List<FolderViewModel>();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                FolderDetails = (from F in db.Folders
                                     join C in db.Clients on F.ClientId equals C.Id
                                     select new FolderViewModel { Id = F.ID, FolderName = F.FolderName, ClientId = F.ClientId, ClientName = C.Name }).ToList();
            }
            return FolderDetails;
        }

        public List<Client> BindClientDetails()
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                var obj = (from C in db.Clients
                           select C ).ToList();
                return obj;
            }
        }

        public int AddFolder(int Id,string FolderName,int ClientId)
        {
            Folder folder = new Folder { ID = Id, FolderName = FolderName, ClientId = ClientId };
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                try
                {
                    db.Folders.Add(folder);
                    int i = db.SaveChanges();

                    return i;
                }
                catch (System.Exception ex)
                {
                    string error = ex.InnerException.InnerException.Message;
                    if (error.Contains("UNIQUE"))
                    {
                        return 2;
                    }
                    else
                    {
                        return 3;
                    }

                }

            }
        }

        public int UpdateFolder(int Id, string FolderName, int ClientId)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                try
                {
                    Folder updatedFolder = (from F in db.Folders
                                            where F.ID == Id
                                            select F).FirstOrDefault();
                    updatedFolder.FolderName = FolderName;
                    updatedFolder.ClientId = ClientId;
                    int i = db.SaveChanges();
                    return i;
                }
                catch (System.Exception ex)
                {

                    string error = ex.InnerException.InnerException.Message;
                    if (error.Contains("UNIQUE"))
                    {
                        return 2;
                    }
                    else
                    {
                        return 3;
                    }

                }

            }
        }

        [HttpPost]
        public int DeleteFolder(int folderId)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                Folder objFolder = (from F in db.Folders
                                   where F.ID == folderId
                                   select F).FirstOrDefault();
                db.Folders.Remove(objFolder);
                int i = db.SaveChanges();
                return i;
            }

        }
    }
}