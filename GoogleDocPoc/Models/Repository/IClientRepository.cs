﻿using GoogleDocPoc.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleDocPoc.Models
{
    public interface IClientRepository
    {
        List<ClientViewModel> GetClientDetails();
        int AddClient(Client client);
        int UpdateClient(int clientId, string ClientName);
        int  DeleteClient(Client client);
    }
}