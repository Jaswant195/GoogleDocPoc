﻿using System.Collections.Generic;

namespace GoogleDocPoc.Models
{
    public interface IFileDetailsRepository
    {
        List<FilePath> GetAllFileDetails(string roleName, int clientId, int userId);
        List<Client> GetAllClientDetails();
        List<FilePath> GetFilesByFileName(int ClientId, int FolderId, string FileName, string FromDate, string ToDate);
        List<Folder> BindFolderDetails(int ClientId);
    }
}
