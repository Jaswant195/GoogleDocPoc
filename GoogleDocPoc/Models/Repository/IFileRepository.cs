﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleDocPoc.Models
{
    public interface IFileRepository 
    {
         List<FileDetails> GetFileDetails();
        List<FileDetails> GetClientDetails();
        List<FileDetails> GetFolderDetails(int Id);
        bool CheckFileExistance(int ClientId, int FolderId, string fileName);
        bool InsertFile(int ClientId, int FolderId, string fileName);
        bool DeleteFileByFileId(int FileId);
        bool UpdateFile(int FileId, int ClientId, int FolderId, string fileName, DateTime? CreatedDate);
        FileDetails GetOldFileDetails(int FileId);
        bool DeleteFileFromFolder(string ClientName, string FolderName, string FileName);
    }
}
