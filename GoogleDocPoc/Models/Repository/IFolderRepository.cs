﻿using GoogleDocPoc.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleDocPoc.Models
{
	public interface IFolderRepository
	{
        List<FolderViewModel> GetFolderDetails();
        List<Client> BindClientDetails();
        int AddFolder(int Id, string FolderName, int ClientId);
        int UpdateFolder(int Id, string FolderName, int ClientId);
        int DeleteFolder(int folderId);
    }
}