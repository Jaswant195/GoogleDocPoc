﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleDocPoc.Models
{
    public interface ILoginRepository
    {
        UserDetails GetUserDetails(User objLogin);
        bool GetUserActivationStatus(int UserId);
        UserDetails GetUserDetailsByEmail(string Email);
        string SendMail(UserDetails userDetails, string Email);
    }
}
