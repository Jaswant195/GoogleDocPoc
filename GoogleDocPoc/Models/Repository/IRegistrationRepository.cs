﻿using GoogleDocPoc.ViewModel;
using System.Collections.Generic;

namespace GoogleDocPoc.Models
{
    public interface IRegistrationRepository
    {
        List<UserDetailsViewModel> GetAllUserDetails(string RoleName, string ClientName, string UserName);
        List<UserDetailsViewModel> getAllRoles(string RoleName);
        UserDetailsViewModel GetUserDetailsByUserId(int UserId);
        bool CheckUserExists(string UserName);
        bool CheckEmailExists(string Email);
        bool InsertUser(string UserName, string Password, string Email, string Role, string Client, string FolderIds);
        bool UpdateUser(string UserId, string UserName, string Password, string Email, string Role, string Client, string FolderIds);
        bool DeleteUserByUserId(int UserId);
        List<FileDetails> GetFolderDetails(int Id);
        List<FileDetails> GetAllClientDetails(string UserName);
    }
}
