﻿using GoogleDocPoc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace GoogleDocPoc.Controllers
{
    public class LoginRepository : ILoginRepository
    {
        public UserDetails GetUserDetails(User objLogin)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                UserDetails obj = (from U in db.Users
                                   join R in db.Roles on U.RoleId equals R.RoleId
                                   join C in db.Clients on U.ClientId equals C.Id
                                   where (U.Username == objLogin.Username && U.Password == objLogin.Password)
                                   select new UserDetails { UserId = U.UserId, RoleName = R.RoleName, ClientName = C.Name, UserName = U.Username, ClientId = C.Id }).FirstOrDefault();
                return obj;
            }
        }

        public bool GetUserActivationStatus(int UserId)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                return db.UserActivations.Any(x => x.UserId == UserId);
            }
        }

        public UserDetails GetUserDetailsByEmail(string Email)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                UserDetails userDetails = (from U in db.Users
                                           join R in db.Roles on U.RoleId equals R.RoleId
                                           join C in db.Clients on U.ClientId equals C.Id
                                           where (U.Email == Email)
                                           select new UserDetails { UserName = U.Username, Password = U.Password }).FirstOrDefault();
                return userDetails;
            }
        }
        public string SendMail(UserDetails userDetails ,string Email)
        {
            MailMessage mailMessage = new MailMessage("aryankaryan11111@gmail.com", Email);

            StringBuilder sbEmailBody = new StringBuilder();
            sbEmailBody.Append("Dear " + userDetails.UserName + ",<br/><br/>");
            sbEmailBody.Append("Your password is : <b>" + userDetails.Password + "</b><br/>");
            sbEmailBody.Append("<br/><br/>");
            sbEmailBody.Append("<b>HCL Technologies</b>");

            mailMessage.IsBodyHtml = true;
            mailMessage.Body = sbEmailBody.ToString();
            mailMessage.Subject = "Reset Your Password";

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            NetworkCredential NetworkCred = new NetworkCredential("aryankaryan11111@gmail.com", "Jashobanta_195");
            smtp.Credentials = NetworkCred;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = true;

            try
            {
                smtp.Send(mailMessage);
                return "Password send to your mail";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

    }
}