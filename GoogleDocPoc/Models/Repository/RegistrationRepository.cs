﻿using System;
using System.Collections.Generic;
using System.Linq;
using GoogleDocPoc.Models;
using GoogleDocPoc.ViewModel;
using System.Data.Entity;

namespace GoogleDocPoc.Controllers
{
    public class RegistrationRepository : IRegistrationRepository
    {
        public List<UserDetailsViewModel> GetAllUserDetails(string RoleName, string ClientName, string UserName)
        {
            List<UserDetailsViewModel> userDetails = new List<UserDetailsViewModel>();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                if(RoleName == "Administrator")
                {
                    userDetails = (from U in db.Users
                                   join R in db.Roles on U.RoleId equals R.RoleId
                                   join C in db.Clients on U.ClientId equals C.Id
                                   where C.Name == ClientName && R.RoleName != "SuperUser" && R.RoleName == "User"
                                   select new UserDetailsViewModel { UserId = U.UserId, Username = U.Username, Password = U.Password, Email = U.Email, ClientId = C.Id, RoleName = R.RoleName, ClientName = C.Name, FolderIds = (U.FolderIds == null ? "No" : U.FolderIds) })
                                   .Concat
                                   (from U in db.Users
                                    join R in db.Roles on U.RoleId equals R.RoleId
                                    join C in db.Clients on U.ClientId equals C.Id
                                    where C.Name == ClientName && R.RoleName == "Administrator" && U.Username == UserName
                                    select new UserDetailsViewModel { UserId = U.UserId, Username = U.Username, Password = U.Password, Email = U.Email, ClientId = C.Id, RoleName = R.RoleName, ClientName = C.Name, FolderIds = (U.FolderIds==null?"No":U.FolderIds) })
                                   .ToList();
                }
                else
                {
                    userDetails = (from U in db.Users
                                   join R in db.Roles on U.RoleId equals R.RoleId
                                   join C in db.Clients on U.ClientId equals C.Id
                                   select new UserDetailsViewModel { UserId = U.UserId, Username = U.Username, Password = U.Password, Email = U.Email, ClientId = C.Id, RoleName = R.RoleName, ClientName = C.Name, FolderIds = U.FolderIds }).ToList();
                }
            }
               
            return userDetails;
        }

        public UserDetailsViewModel GetUserDetailsByUserId(int UserId)
        {
            UserDetailsViewModel userDetails = new UserDetailsViewModel();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                
                    userDetails = (from U in db.Users
                                   join R in db.Roles on U.RoleId equals R.RoleId
                                   join C in db.Clients on U.ClientId equals C.Id
                                   where U.UserId==UserId
                                   select new UserDetailsViewModel { UserId = U.UserId, Username = U.Username, Password = U.Password,ConfirmPassword = U.Password, Email = U.Email, ClientId = C.Id, RoleName = R.RoleName, ClientName = C.Name, FolderIds = U.FolderIds }).FirstOrDefault();
            }

            return userDetails;
        }

        public List<FileDetails> GetFolderDetails(int Id)
        {
            List<FileDetails> FolderDetails = new List<Models.FileDetails>();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                FolderDetails = (from F in db.Folders
                                 where F.ClientId == Id
                                 select new FileDetails { FolderId = F.ID, FolderName = F.FolderName }).ToList();
            }
            return FolderDetails;
        }

        public bool InsertUser(string UserName, string Password, string Email,string Role, string Client, string FolderIds)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                User objUser = new User
                {
                    UserId = 0,
                    Username = UserName,
                    Password = Password,
                    Email=Email,
                    CreatedDate = DateTime.Now,
                    LastLoginDate = DateTime.Now,
                    RoleId=Convert.ToInt32(Role),
                    ClientId= Convert.ToInt32(Client),
                    FolderIds=FolderIds
                };

                try
                {
                    db.Users.Add(objUser);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public bool UpdateUser(string UserId, string UserName, string Password, string Email, string Role, string Client, string FolderIds)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                User objUser = new User
                {
                    UserId = Convert.ToInt32(UserId),
                    Username = UserName,
                    Password = Password,
                    Email = Email,
                    CreatedDate = DateTime.Now,
                    LastLoginDate = DateTime.Now,
                    RoleId = Convert.ToInt32(Role),
                    ClientId = Convert.ToInt32(Client),
                    FolderIds = FolderIds
                };

                try
                {
                    db.Users.Add(objUser);
                    db.Entry(objUser).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public bool DeleteUserByUserId(int UserId)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                var objUser = (from User in db.Users
                               where User.UserId == UserId
                               select User).FirstOrDefault();
                db.Users.Remove(objUser);
                db.SaveChanges();
                return true;
            }
        }

        public List<FileDetails> GetAllClientDetails(string UserName)
        {
            List<FileDetails> FileDetails = new List<Models.FileDetails>();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                string RoleName = (from U in db.Users
                                   join R in db.Roles on U.RoleId equals R.RoleId
                                   where U.Username == UserName
                                   select R.RoleName).FirstOrDefault();
                int? ClientId =     (from U in db.Users
                                   where U.Username == UserName
                                   select U.ClientId).FirstOrDefault();

                if (RoleName == "SuperUser")
                {
                    FileDetails = (from C in db.Clients
                                   select new FileDetails { ClientId = C.Id, ClientName = C.Name }).ToList();
                }
                else
                {
                    FileDetails = (from C in db.Clients
                                   where C.Id == ClientId
                                   select new FileDetails { ClientId = C.Id, ClientName = C.Name }).ToList();
                }
            }
            return FileDetails;
        }

        public List<UserDetailsViewModel> getAllRoles(string RoleName)
        {
            List<UserDetailsViewModel> lstRoles = new List<UserDetailsViewModel>();
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                if(RoleName == "SuperUser")
                {
                    lstRoles = (from R in db.Roles
                                select new UserDetailsViewModel { RoleId = R.RoleId, RoleName = R.RoleName }).ToList();
                }
                if (RoleName == "Administrator")
                {
                    lstRoles = (from R in db.Roles
                                where R.RoleName == "Administrator" || R.RoleName == "User"
                                select new UserDetailsViewModel { RoleId = R.RoleId, RoleName = R.RoleName }).ToList();
                }
               
            }
            return lstRoles;
        }

        public bool CheckUserExists(string UserName)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                if (db.Users.Where(x => x.Username == UserName).Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool CheckEmailExists(string Email)
        {
            using (GoogleDocsContext db = new GoogleDocsContext())
            {
                if (db.Users.Where(x => x.Email == Email).Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}