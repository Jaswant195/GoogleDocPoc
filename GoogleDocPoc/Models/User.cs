using System;
using System.Collections.Generic;

namespace GoogleDocPoc.Models
{
    public partial class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> LastLoginDate { get; set; }
        public Nullable<int> RoleId { get; set; }
        public Nullable<int> ClientId { get; set; }
        public string FolderIds { get; set; }
        public virtual Role Role { get; set; }
    }
}
