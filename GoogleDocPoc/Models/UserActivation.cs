using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GoogleDocPoc.Models
{
    public partial class UserActivation
    {
        [Key]
        public int UserId { get; set; }
        public System.Guid ActivationCode { get; set; }
    }
}
