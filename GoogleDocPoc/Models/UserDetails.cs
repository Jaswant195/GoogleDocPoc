﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleDocPoc.Models
{
    public class UserDetails
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string Password { get; set; }
    }
}