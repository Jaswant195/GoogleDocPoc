using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GoogleDocPoc.Models
{
    public partial class UserFolder
    {
        [Key]
        public int UserId { get; set; }
        public string FolderName { get; set; }
    }
}
