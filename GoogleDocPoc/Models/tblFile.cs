using System;
using System.Collections.Generic;

namespace GoogleDocPoc.Models
{
    public partial class tblFile
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Data { get; set; }
    }
}
