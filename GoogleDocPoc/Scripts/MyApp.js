﻿var app = angular.module('myApp', ['angularTreeview']);
app.controller('myController', ['$scope', '$http', function ($scope, $http) {
    $http.get('/FileDetails/GetFileStructure').then(function (response) {
       $scope.list = response.data.treelist;
    })
}])