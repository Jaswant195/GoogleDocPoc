﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleDocPoc.ViewModel
{
    public class ClientViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HdnName { get; set; }
        public object ErrorMessage { get; set; }

        //pagination
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int PagerCount { get; set; }

        public List<ClientViewModel> Clients { get; set; }
    }
}