﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleDocPoc.ViewModel
{
    public class FolderViewModel
    {
        public int Id { get; set; }
        public string FolderName { get; set; }
        public string HdnFolderName { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string HdnClientName { get; set; }
        public object ErrorMessage { get; set; }

        //pagination
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int PagerCount { get; set; }

        public List<FolderViewModel> Folders { get; set; }

    }
}