﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoogleDocPoc.ViewModel
{
    public class UserDetailsViewModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Password and Confirmation Password must match.")]
        public string ConfirmPassword { get; set; }
        public string Email { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> LastLoginDate { get; set; }
        public Nullable<int> RoleId { get; set; }
        public string RoleName { get; set; }
        public Nullable<int> ClientId { get; set; }
        public string ClientName { get; set; }
        public string FolderIds { get; set; }

        //pagination
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int PagerCount { get; set; }
        public List<UserDetailsViewModel> userDetailsViewModel { get; set; }
    }
}